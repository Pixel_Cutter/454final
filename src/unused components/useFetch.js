import { useState, useEffect } from "react";

export default function useFetch(url, queryObj) {
  const [newQueryObj, setNewQueryObj] = useState(queryObj);

  const options = {
    method: "post",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(queryObj)
  };

  useEffect(() => {
    const getRecipes = async () => {
      fetch(url, options)
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          setNewQueryObj(data);
        });
    };
    getRecipes();
  }, [url, queryObj.query, queryObj.pageNum]);

  return newQueryObj;
}

/**  const result = await fetch(url, requestOptions)
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      return data;
    }); */
