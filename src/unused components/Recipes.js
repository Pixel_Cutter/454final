import RecipeCard from "./RecipeCard";
import { useState } from "react";
import NoResults from "./NoResults";
import "./Recipes.css";


export default function Recipes({ recipes, query, incPage }) {
  if (recipes.length > 0) {
    return (
        <div className="row row-cols-2 justify-content-md-center fuck">
          {recipes.map((recipe, i) => {
            return <RecipeCard key={recipe.id} recipe={recipe} />;
          })}
        </div>
    );
  }
  return null
}

  //return <NoResults query={query} />;