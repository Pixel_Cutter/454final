import React, { useState } from "react";

export default function SearchBar({ onSearch }) {
  const [query, setQuery] = useState("");

  function getText(e) {
    setQuery(e.target.value);
  }

  function keyPressed(event) {
    if (event.keyCode === 13) {
      newSearch();
    }
  }

  function newSearch() {
    onSearch(query);
    setQuery("");
  }

  return (
    <nav className="navbar sticky-top navbar-light bg-light">
      <div className="container-fluid">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            value={query}
            placeholder="Search for the Perfect Recipe"
            onChange={getText}
            onKeyDown={keyPressed}
          />
          <button
            className="btn btn-success btn-md-lg"
            type="submit"
            onClick={newSearch}
          >
            <i className="bi bi-search"></i>
          </button>
        </div>
      </div>
    </nav>
  );
}
