import "./styles.css";
import React, { useState, useEffect } from "react";
import MyNav from "./components/Navigation/MyNav";
import NoResults from "./components/NoResults";
import { getRecipes } from "./hooks/API";
import InfiniteScroll from "react-infinite-scroll-component";
import RecipeCard from "./components/RecipeCard";

export default function App() {
  const searchURL = "http://127.0.0.1:5000/search";
  const popURL = "http://127.0.0.1:5000/popular";

  const [newQuery, setNewQuery] = useState("chicken");
  const [newSearch, setNewSearch] = useState(false);
  const [pageNum, setPageNum] = useState(1);
  const [recipes, setRecipes] = useState([]);

  const [isLoading, setLoading] = useState(true);
  const [hasMore, setHasMore] = useState(true);

  const [popChecked, setPopChecked] = useState(false);
  const [popRecipes, setPopRecipes] = useState([]);
  const [lastPop, setLastPop] = useState(10);

  const [exFilters, setExFilters] = useState(false);
  const [filterObj, setFilterObj] = useState({
    option: "OR",
    newQuery: "chicken",
    excludeList: [],
    clrClicked: false,
  });

  useEffect(() => {
    setRecipes([]);
    setPageNum(1);
    setNewQuery(filterObj.newQuery);
    setExFilters(Boolean(filterObj.excludeList.length));
  }, [filterObj]);

  useEffect(() => {
    setRecipes(popRecipes.slice(0, lastPop));
  }, [popRecipes]);

  useEffect(() => {
    const loadRecipes = async () => {
      setLoading(true);
      if (!popChecked) {
        const newQueryObj = await getRecipes(searchURL, {
          query: newQuery,
          pageNum: pageNum,
          limit: 10,
          disallow: filterObj.excludeList,
          option: filterObj.option,
        });
        setHasMore(newQueryObj.recipes.length >= 10);
        setRecipes((prev) => [...prev, ...newQueryObj.recipes]);
      } else {
        const newQueryObj = await getRecipes(popURL, {
          query: newQuery,
          dissallow: filterObj.exList,
          option: filterObj.option,
        });
        setPopRecipes(newQueryObj.recipes);
      }
      setLoading(false);
    };
    loadRecipes();
  }, [newQuery, pageNum, popChecked, exFilters]);

  // resets pageNum to 1 and then sets new query
  function getNewQuery(query) {
    setRecipes([]);
    setPageNum(1);
    setNewSearch(true);
    setNewQuery(query);
  }

  // increments page when more results needed
  function incPage() {
    setPageNum(pageNum + 1);
  }

  // handles when user checks the filter by popular feature
  function popCheckedHandler() {
    setRecipes([]);
    setPopChecked(!popChecked);
    setPageNum(1);
  }

  // grabs next 10 recipes from the popular recipe array
  function slicePop() {
    setLastPop(lastPop + 10);
    setRecipes((prev) => [
      ...prev,
      ...popRecipes.splice(lastPop, lastPop + 10),
    ]);
  }

  function getFilters(queryObj) {
    setNewSearch(false)
    setFilterObj((prev) => {
      return {
        ...prev,
        newQuery: queryObj.newQuery,
        excludeList: queryObj.excludeList,
      };
    });
    console.log("from getFilters");
    console.log(queryObj);
  }

  return (
    <div className="container bg-light">
      <MyNav
        onSearchClicked={getNewQuery}
        onPopChecked={popCheckedHandler}
        popChecked={popChecked}
        sendQuery={getFilters}
        appQuery={newQuery}
      />
      {recipes.length > 0 ? (
        <InfiniteScroll
          dataLength={recipes.length}
          next={popChecked ? slicePop : incPage}
          hasMore={hasMore}
        >
          <div className="row row-cols-2 justify-content-md-center">
            {recipes.map((recipe, i) => {
              return <RecipeCard key={recipe.id} recipe={recipe} />;
            })}
          </div>
        </InfiniteScroll>
      ) : isLoading ? (
        <div>Loading...</div>
      ) : (
        <NoResults />
      )}
    </div>
  );
}
