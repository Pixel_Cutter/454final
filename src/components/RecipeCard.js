import React, { useState, useEffect } from "react";
import "./RecipeCard.css";
import defaultImg from "../images/default.png";
import RecipeModal from "./RecipeModal";
import Badge from "react-bootstrap/Badge";
import { getRecipes } from "../hooks/API";

export default function RecipeCard({ recipe }) {
  const [likes, setLikes] = useState(recipe.likes);
  const currRecipe = recipe;

  function loadRecipes() {
    console.log("ping")
    getRecipes(
      "http://127.0.0.1:5000/update",
      currRecipe
    );
  }

  // if image link is allrecipe default image, change to Dish Central default image
  const image =
    recipe.imgLink === "/img/icons/generic-recipe.svg"
      ? defaultImg
      : recipe.imgLink;

  const [showModal, setModal] = useState(false);
  function handleClose() {
    setModal(!showModal);
  }

  function updateLikes() {
    setLikes(likes + 1)
    loadRecipes();
  }

  return (
    <>
      <div className="col-sm-auto p-3 m-1" onClick={handleClose}>
        <div className="card card-point" style={{ width: "18rem" }}>
          {showModal && (
            <RecipeModal
              recipe={recipe}
              updateLikes={updateLikes}
              onClick={handleClose}
            />
          )}
          <img src={image} className="card-img-top img-fit" alt="" />
          <div className="card-body card-body-colors">
            <p className="card-title my-bold">{recipe.title}</p>
            <Badge bg="success">
              <span className="inbtn">
                <i className=" bi bi-hand-thumbs-up-fill"></i>
                <span className="mx-1" />
                {likes}
              </span>
            </Badge>
            <p className="card-text trunc-text pad-top">{recipe.summary}</p>
          </div>
        </div>
      </div>
    </>
  );
}
