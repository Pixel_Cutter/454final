import React from "react";
import noresults from "../images/noresults.png";
import "./NoResults.css";

export default function NoResults() {
  return (
    <div className="my-container">
    <div className="container center-msg">
      <div className="my-4"></div>
      <div className="text-center">
        <h1>Sorry, No Results Found</h1>
        <img className="img-fluid small-boi" src={noresults} alt="" />
      </div>
    </div>
    </div>
  );
}
