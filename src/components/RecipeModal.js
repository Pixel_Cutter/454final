import Modal from "react-bootstrap/Modal";
import Image from "react-bootstrap/Image";
import Button from "react-bootstrap/Button";
import defaultImg from "../images/default.png";
import {useState} from 'react'
import "./RecipeModal.css";

export default function RecipeModal({ recipe, onClick, updateLikes }) {
  const [likes, setLikes] = useState(recipe.likes)
  const [isDisabled, setDisabled] = useState(false)
  const ingredients = recipe.ingredients.split("*");
  const steps = recipe.steps.split("*");

  const image =
    recipe.imgLink === "/img/icons/generic-recipe.svg"
      ? defaultImg
      : recipe.imgLink;

  function updateLikesHandler() {
    setLikes(likes + 1)
    updateLikes();
  }

  return (
    <>
      <Modal show={true} size="lg" centered onHide={onClick} backdrop="static" keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title>{recipe.title}</Modal.Title>
          <div className="mx-4">
          <Button onClick={updateLikesHandler} variant="success">
            <span>
              <i className="bi bi-hand-thumbs-up-fill"></i>
              <span className="mx-1" />
              {likes}
            </span>
          </Button>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Image src={image} fluid />
          <div>
            <p className="h4">{recipe.summary}</p>
          </div>
          <h1>Ingredients</h1>
          <div>
            {ingredients.map((ingredient, i) => (
              <h4 key={i}>{ingredient}</h4>
            ))}
          </div>
          <h1>Steps</h1>
          <div>
            {steps.map((step, i) => (
              <h4 key={i}>{step}</h4>
            ))}
          </div>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}
