import { Navbar, Container, FormControl, Button, Form } from "react-bootstrap";
import { useState } from "react";
import OptionsForm from "./OptionsForm";
import FilterForm from "../FilterForm/FilterForm";

export default function MyNav({
  onSearchClicked,
  onPopChecked,
  sendQuery,
  appQuery,
}) {
  const [query, setQuery] = useState("");

  function popCheckedHandler() {
    onPopChecked();
  }

  function getText(event) {
    setQuery(event.target.value);
  }

  function keyPressed(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      newSearch();
    }
  }

  function newSearch() {
    onSearchClicked(query);
    setQuery("");
  }

  function addFilter(queryObj) {
    sendQuery(queryObj);
  }

  return (
    <>
      <Navbar fixed="top" bg="light" expand="lg">
        <Container fluid>
          <Navbar.Brand href="#">Dish Central</Navbar.Brand>
          <Form className="d-flex">
            <FormControl
              type="input"
              value={query}
              placeholder="Search for the Perfect Recipe"
              className="me-2"
              aria-label="Search"
              onChange={getText}
              onKeyDown={keyPressed}
            />
            <Button variant="outline-success" onClick={newSearch}>
              Search
            </Button>
          </Form>
          <OptionsForm onPopChecked={popCheckedHandler} />
        </Container>
        <FilterForm query={appQuery} filtersAdded={addFilter} />
      </Navbar>
    </>
  );
}
