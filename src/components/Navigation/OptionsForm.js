import React, { useState } from "react";
import {
  Form,
  Row,
  Col,
  InputGroup,
  Button,
  ToggleButton,
} from "react-bootstrap";
import FilterForm from "../FilterForm/FilterForm";

function OptionsForm({ onPopChecked }) {
  const [checked, setChecked] = useState(false);
  let query = "hello";
  function addFilter() {}

  function popCheckedHandler() {
    setChecked(!checked);
    onPopChecked();
  }

  return (
    <Form>
      <Row>
        <Col>
          <Form.Label>Search Options</Form.Label>
          <InputGroup>
            <ToggleButton
              id="toggle-check"
              className="mb-2"
              variant="outline-primary"
              onChange={popCheckedHandler}
              type="checkbox"
              checked={checked}
            >
              Search by Popular
            </ToggleButton>
          </InputGroup>
        </Col>
      </Row>
    </Form>
  );
}

export default OptionsForm;
