import React, { useState } from "react";
import { InputGroup, Button, FormControl } from "react-bootstrap";

export default function AddFilterBtn({ addFilter, isInclude, title }) {
  const [newFilter, setNewFilter] = useState("");

  function updateFilter(e) {
    setNewFilter(e.target.value);
  }

  function submitFilter() {
    // do nothing if input consists of nothing but whitespace
    if (newFilter.length === 0 || /^\s+$/.test(newFilter)) return;
    const filterObj = {
      option: newFilter.toLowerCase(),
      include: isInclude,
      id: Math.floor(Math.random() * 10000)
    };
    setNewFilter("");
    addFilter(filterObj);
  }

  return (
    <InputGroup className="mb-3">
      <Button
        onClick={submitFilter}
        variant={isInclude ? "success" : "danger"}
        id="button-addon1"
      >
        {title}
      </Button>
      <FormControl
        aria-label="Example text with button addon"
        aria-describedby="basic-addon1"
        onChange={updateFilter}
        value={newFilter}
      />
    </InputGroup>
  );
}
