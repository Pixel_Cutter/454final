import { DropdownButton, Dropdown } from "react-bootstrap";

export default function DietDropdown({ addFilter }) {
  function dietHandler(e) {
    const newFilter = {
      option: e,
      include: true,
      id: Math.floor(Math.random() * 10000)
    };
    addFilter(newFilter);
  }

  return (
    <div className="mt-3">
      <DropdownButton
        id="dropdown-button"
        title="Include Diet"
        variant="success"
        onSelect={dietHandler}
      >
        <Dropdown.Item eventKey="vegan">Vegan</Dropdown.Item>
        <Dropdown.Item eventKey="paleo">Paleo</Dropdown.Item>
        <Dropdown.Item eventKey="keto">Keto</Dropdown.Item>
        <Dropdown.Item eventKey="low-carb">Low-Carb</Dropdown.Item>
        <Dropdown.Item eventKey="gluten-free">Gluten-Free</Dropdown.Item>
        <Dropdown.Item eventKey="sugar-free">Sugar-Free</Dropdown.Item>
      </DropdownButton>
    </div>
  );
}
