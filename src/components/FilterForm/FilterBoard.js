import { Card } from "react-bootstrap";
import FilterCard from "./FilterCard";

export default function FilterBoard({ optionList, removeFilterOption }) {
  function rmFilter(filterOption) {
    removeFilterOption(filterOption);
  }
  return (
    <Card>
      <Card.Body>
        {optionList.map((option) => (
          <FilterCard
            key={option.id}
            rmFilter={rmFilter}
            filterOption={option}
          />
        ))}
      </Card.Body>
    </Card>
  );
}
