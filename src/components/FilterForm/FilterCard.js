import { Badge } from "react-bootstrap";
import "./FilterBoard.css";

export default function FilterCard({ filterOption, rmFilter }) {
  function clickHandler() {
    rmFilter(filterOption);
  }
  return (
    <div onClick={clickHandler} className="option-card d-grid gap-2">
      <Badge bg={filterOption.include ? "success" : "danger"}>
        {filterOption.option}
      </Badge>
    </div>
  );
}
