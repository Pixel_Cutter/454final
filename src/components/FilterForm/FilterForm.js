import { Accordion, Button, Container, Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";
import FilterBoard from "./FilterBoard";
import AddFilterBtn from "./AddFilterBtn";
import DietDropdown from "./DietDropdown";

export default function FilterForm({ filtersAdded, query, isNewSearch }) {
  console.log("from FilterForm: " + query)
  const [filters, setFilters] = useState([]);
  const [ogQuery, setOGQuery] = useState(query);
  
  if(isNewSearch) setOGQuery(query)

  useEffect(() => {
    let newQuery = ogQuery;
    for (let i = 0; i < filters.length; i++) {
      if (filters[i].include === true) newQuery += " " + filters[i].option;
    }

    let excludeList = []
    filters.forEach((option) => {
      console.log(option)
      if (option.include === false) excludeList.push(option.option);
    });

    if (filters.length)
      sendQuery({
        newQuery: newQuery,
        excludeList: excludeList,
        clrClicked: false,
      });
    else {
      sendQuery({
        newQuery: ogQuery,
        excludeList: excludeList,
        clrClicked: true,
      });
    }
  }, [filters]);

  function sendQuery(queryObj) {
    filtersAdded(queryObj);
  }

  function removeFilterOption(filterOption) {
    setFilters(filters.filter((option) => option.id !== filterOption.id));
  }

  function addFilter(newFilter) {
    setFilters((prev) => [...prev, newFilter]);
  }

  function clearHandler() {
    if (filters.length) setFilters([]);
  }

  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header as="h1">Filter Search Options</Accordion.Header>
        <Accordion.Body>
          <Container>
            <Row>
              <Col>
                <AddFilterBtn
                  addFilter={addFilter}
                  isInclude={false}
                  title={"Exclude"}
                />
                <AddFilterBtn
                  addFilter={addFilter}
                  isInclude={true}
                  title={"Include"}
                />

                <DietDropdown addFilter={addFilter} />
              </Col>
              <Col>
                <FilterBoard
                  removeFilterOption={removeFilterOption}
                  optionList={filters}
                />
                <div className="mt-3">
                  <Button size="sm" onClick={clearHandler} variant="secondary">
                    Clear Filters
                  </Button>
                </div>
              </Col>
            </Row>
          </Container>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
