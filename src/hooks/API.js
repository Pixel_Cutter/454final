export async function getRecipes(url, queryObj) {
  const options = {
    method: "post",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(queryObj),
  };
  const newQueryObj = await fetch(url, options)
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      return data;
    });
  return newQueryObj;
}
